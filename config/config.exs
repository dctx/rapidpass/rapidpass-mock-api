# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :rapidpass,
  namespace: RapidPass

# Configures the endpoint
config :rapidpass, RapidPassWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "PbsJh8xoPgYMmZdD2PmxWIEeQM4XC1XjCPS5eoa35VELaIrbAuWUoHTp1b0TTO89",
  render_errors: [view: RapidPassWeb.ErrorView, accepts: ~w(json), layout: false],
  pubsub_server: RapidPass.PubSub,
  live_view: [signing_salt: "wNk+QbSn"]

# Configures Elixir's Logger
elixir_logger_level = System.get_env("ELIXIR_LOGGER_LEVEL", "debug")

level =
  %{
    "1" => :debug,
    "2" => :info,
    "3" => :warn,
    "debug" => :debug,
    "info" => :info,
    "warn" => :warn
  }
  |> Map.get(String.downcase(elixir_logger_level), :debug)

config :logger, :console,
  level: level,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id, :pid]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
