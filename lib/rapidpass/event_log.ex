defmodule RapidPass.EventLog do
  use GenServer

  require Logger

  @spec start_link(any) :: :ignore | {:error, any} | {:ok, pid}
  def start_link(initial_list \\ []) do
    Logger.debug("#{__MODULE__}.start_link(#{inspect(initial_list)})")
    GenServer.start_link(__MODULE__, initial_list, name: __MODULE__)
  end

  # Callbacks

  @impl true
  @spec init(list()) :: {:ok, list()}
  def init(list) do
    {:ok, list}
  end

  @impl true
  def handle_call({:list, since}, _from, all_events) do
    before_since = &(&1.timestamp < since)

    events_since =
      all_events
      |> Enum.reverse()
      |> Enum.drop_while(before_since)

    {:reply, events_since, all_events}
  end

  @impl true
  def handle_call({:reject, control_code}, _from, all_events) do
    event = RapidPass.Event.new(control_code)
    new_all_events = [event | all_events]
    {:reply, event, new_all_events}
  end

  # Client APIs

  @spec list_events_since(integer()) :: list()
  def list_events_since(since) do
    GenServer.call(__MODULE__, {:list, since})
  end

  @spec reject(control_code :: String.t()) :: RapidPass.Event.t()
  def reject(control_code) do
    GenServer.call(__MODULE__, {:reject, control_code})
  end
end
