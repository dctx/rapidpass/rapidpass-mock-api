defmodule RapidPass.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @spec start(Application.start_type(), term()) ::
          {:error, reason :: term()} | {:ok, pid()} | {:ok, pid(), Application.state()}
  def start(_type, _args) do
    children = [
      RapidPass.EventLog,
      # Start the Telemetry supervisor
      RapidPassWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: RapidPass.PubSub},
      # Start the Endpoint (http/https)
      RapidPassWeb.Endpoint
      # Start a worker by calling: RapidPass.Worker.start_link(arg)
      # {RapidPass.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: RapidPass.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @spec config_change(changed, new, removed) :: :ok
        when changed: keyword(), new: keyword(), removed: [atom()]
  def config_change(changed, _new, removed) do
    RapidPassWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
