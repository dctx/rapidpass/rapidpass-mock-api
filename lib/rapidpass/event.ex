defmodule RapidPass.Event do

  @derive Jason.Encoder
  defstruct [:timestamp, :control_code]

  @type t :: %RapidPass.Event{}

  @spec new(control_code :: String.t()) :: %RapidPass.Event{}
  def new(control_code) do
    current_timestamp = :os.system_time(:second)
    %RapidPass.Event{timestamp: current_timestamp, control_code: control_code}
  end
end
