defmodule RapidPassWeb.ApiSchema do
  alias OpenApiSpex.Schema

  defmodule AddRevocationEventInput do
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "AddRevocationEventInput",
      description: "A revocation event to be posted",
      type: :object,
      properties: %{
        controlCode: %Schema{type: :string, description: "The Control Code", example: "35YBRZ6E"}
      },
      required: [:controlCode],
      example: %{
        "controlCode" => "35YBRZ6E"
      }
    })
  end

  defmodule RevocationEvent do
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "RevocationEvent",
      description: "A revocation event",
      type: :object,
      properties: %{
        timestamp: %Schema{
          type: :integer,
          description: "The event timestamp, as seconds after epoch",
          example: 1_588_599_841
        },
        eventType: %Schema{
          type: :string,
          description: "The event type, for now, only \"RapidPassRevoked\" is supported",
          example: "RapidPassRevoked"
        },
        controlCode: %Schema{type: :string, description: "The Control Code", example: "35YBRZ6E"}
      },
      required: [:timestamp, :controlCode],
      example: %{
        "controlCode" => "35YBRZ6E",
        "timestamp" => 1_588_599_841
      }
    })
  end

  defmodule RevocationLog do
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "RevocationLog",
      description: "The revoation log",
      type: :array,
      items: RevocationEvent,
      example: [
        %{
          "controlCode" => "35YBRZ6E",
          "timestamp" => 1_588_599_841
        }
      ]
    })
  end

  defmodule RevocationLogMetadata do
    require OpenApiSpex

    OpenApiSpex.schema(%{
      type: :object,
      properties: %{
        totalEvents: %Schema{
          type: :integer,
          description: "The total number of all events in the log",
          example: 8_432
        },
        after: %Schema{
          type: :integer,
          description: "The event id passed as the parameter to this query",
          example: 147
        },
        remainingEvents: %Schema{
          type: :integer,
          description: "The remaining number of events after the first event",
          example: 8285
        },
        size: %Schema{
          type: :integer,
          description: "The number of events per page",
          example: 1000
        }
      }
    })
  end

  defmodule RevocationLogResponse do
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "RevocationLogResponse",
      description: "Response the validation logs",
      type: :object,
      properties: %{
        meta: RevocationLogMetadata,
        data: RevocationLog
      },
      example: %{
        "metadata" => %{
          "totalEvents" => 148,
          "after" => 147,
          "remainingEvents" => 0,
          "size" => 1000
        },
        "data" => [
          %{
            "controlCode" => "35YBRZ6E",
            "timestamp" => 1_588_599_841
          }
        ]
      }
    })
  end
end
