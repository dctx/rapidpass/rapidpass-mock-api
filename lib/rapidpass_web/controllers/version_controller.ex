defmodule RapidPassWeb.VersionController do
  use RapidPassWeb, :controller

  @version Mix.Project.config()[:version]

  @spec get_version(Plug.Conn.t(), any) :: Plug.Conn.t()
  def get_version(conn, _param) do
    text(conn, "#{@version}\n")
  end
end
