defmodule RapidPassWeb.ApiController do
  use RapidPassWeb, :controller

  alias OpenApiSpex.Operation
  alias OpenApiSpex.Schema
  alias RapidPassWeb.ApiSchema.AddRevocationEventInput
  alias RapidPassWeb.ApiSchema.RevocationLogResponse
  alias RapidPassWeb.ApiSchema.RevocationEvent

  require Logger

  plug OpenApiSpex.Plug.CastAndValidate

  @spec open_api_operation(atom) :: Operation.t()
  def open_api_operation(action) do
    Logger.debug("action => #{action}")
    operation = String.to_existing_atom("#{action}_operation")
    Logger.debug("operation => #{operation}")
    apply(__MODULE__, operation, [])
  end

  @spec get_revocation_log_operation() :: Operation.t()
  def get_revocation_log_operation() do
    %Operation{
      tags: ["rapidpass"],
      summary: "revocations",
      description: "Revocations log",
      operationId: "ApiController.get_revocation_log",
      parameters: [
        Operation.parameter(
          :after,
          :query,
          %Schema{type: :integer, default: 0},
          "The last event id the caller successfully received",
          example: 1234,
          required: false
        ),
        Operation.parameter(
          :"page-size",
          :query,
          %Schema{type: :integer, default: 1000},
          "Number of events to return per page",
          example: 1000,
          required: false,
          default: 1000
        )
      ],
      responses: %{
        200 => Operation.response("Revocations", "application/json", RevocationLogResponse)
      }
    }
  end

  @spec post_revocation_event_operation() :: Operation.t()
  def post_revocation_event_operation() do
    %Operation{
      tags: ["rapidpass"],
      summary: "revocations",
      description: "Add Revocation Event",
      operationId: "ApiController.post_revocation_event",
      requestBody:
        Operation.request_body(
          "The control code",
          "application/json",
          AddRevocationEventInput,
          required: true
        ),
      responses: %{
        200 => Operation.response("RevocationEvent", "application/json", RevocationEvent)
      }
    }
  end

  @spec get_revocation_log(Plug.Conn.t(), any) :: Plug.Conn.t()
  def get_revocation_log(conn, %{since: since} = params) do
    Logger.debug("params => #{inspect(params)}")
    Logger.debug("since => #{since}")

    conn
    |> put_status(:ok)
    |> json(RapidPass.EventLog.list_events_since(since))
  end

  @spec post_revocation_event(Plug.Conn.t(), any) :: Plug.Conn.t()
  def post_revocation_event(%Plug.Conn{body_params: body_params} = conn, _params) do
    with %{controlCode: control_code} <- body_params do
      Logger.debug("control_code => \"#{control_code}\"")
      event = RapidPass.EventLog.reject(control_code)

      conn
      |> put_status(:ok)
      |> json(event)
    else
      _ -> put_status(conn, :bad_request)
    end
  end
end
