defmodule RapidPassWeb.Router do
  use RapidPassWeb, :router

  pipeline :api do
    plug OpenApiSpex.Plug.PutApiSpec, module: RapidPassWeb.ApiSpec
    plug :accepts, ["json"]
  end

  scope "/api/v1" do
    pipe_through :api
    get("/openapi", OpenApiSpex.Plug.RenderSpec, [])
  end

  scope "/api/v1", RapidPassWeb do
    pipe_through :api

    get "/version", VersionController, :get_version
    get "/checkpoint/revocations", ApiController, :get_revocation_log
    post "/checkpoint/revocations", ApiController, :post_revocation_event
  end

  scope "/" do
    get "/", OpenApiSpex.Plug.SwaggerUI, path: "/api/v1/openapi"
  end
end
