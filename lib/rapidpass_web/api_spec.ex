defmodule RapidPassWeb.ApiSpec do
  alias OpenApiSpex.{Info, OpenApi, Paths, Server}
  alias RapidPassWeb.{Endpoint, Router}
  @behaviour OpenApi

  @version Mix.Project.config()[:version]

  @impl OpenApi
  @spec spec :: OpenApiSpex.OpenApi.t()
  def spec do
    %OpenApi{
      servers: [
        %Server{
          url: "https://courteous-mortified-borderterrier.gigalixirapp.com",
          description: "Gigalixir Free tier"
        },
        # Populate the Server info from a phoenix endpoint
        Server.from_endpoint(Endpoint)
      ],
      info: %Info{
        title: "RapidPass API",
        version: @version
      },
      # Populate the paths from a phoenix router
      paths: Paths.from_router(Router)
    }
    # Discover request/response schemas from path specs
    |> OpenApiSpex.resolve_schema_modules()
  end
end
