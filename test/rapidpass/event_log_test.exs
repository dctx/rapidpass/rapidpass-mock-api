defmodule RapidPass.EventLogTest do
  use ExUnit.Case

  alias RapidPass.Event
  alias RapidPass.EventLog

  test "handle_call({:reject, control_code}, ...) works" do
    assert {:reply, event, list} = EventLog.handle_call({:reject, "35YBRZ6E"}, self(), [])
    assert [^event] = list
    assert %Event{timestamp: _ts, control_code: "35YBRZ6E"} = event
  end
  test "handle_call({:reject, control_code}, ...) works with non-empty list" do
    existing_event = Event.new("ABC12345")
    existing_list = [existing_event]
    assert {:reply, event, new_list} = EventLog.handle_call({:reject, "35YBRZ6E"}, self(), existing_list)
    assert [^event, ^existing_event] = new_list
    assert %Event{timestamp: _ts, control_code: "35YBRZ6E"} = event
  end

  test "handle_call({:list, since}, ...) works" do
    existing_list = [
      %Event{timestamp: 1588653001, control_code: "35YBRZ6E"},
      %Event{timestamp: 1588652999, control_code: "ABC12345"},
    ]
    list_in_order = Enum.reverse(existing_list)
    assert {:reply, ^list_in_order, ^existing_list} = EventLog.handle_call({:list, 0}, self(), existing_list)
    assert {:reply, results, ^existing_list} = EventLog.handle_call({:list, 1588653000}, self(), existing_list)
    assert [%Event{timestamp: 1588653001, control_code: "35YBRZ6E"}] = results
  end

  test "client API to live GenServer" do
    event0 = %Event{timestamp: 1588652999, control_code: "ABC12345"}
    {:ok, pid} = EventLog.start_link([event0])
    assert [^event0] = EventLog.list_events_since(0)
    assert event1 = EventLog.reject("35YBRZ6E")
    assert %Event{control_code: "35YBRZ6E"} = event1
    assert [^event0, ^event1] = EventLog.list_events_since(0)
    assert [^event1] = EventLog.list_events_since(1588653000)
  end

end
